# TypeScript seed 

This project contains the basics to develop a TypeScript aplication for NodeJs executed in client side (not browser). It contains only the basics to simplify as much as possible the content of this seed project.


## Commands

- **build** : create a bundle to distribute the project under the folder build
- **start** : start the development environment which watch under folder src each change in .ts files
- **start:build** : start the distributed bundle (first need to execute **build** command)


## Files

- ./src/main.ts : init project file
- nodemon.json  : config file for watcher library
- tsconfi.json  : config file for TypeScript compiler


## Libraries

- **lodash**            : "^4.17.11"
- **@types/lodash**     : "^4.14.129",
- **@types/node**       : "^12.0.2",
- **nodemon**           : "^1.19.0",
- **ts-node**           : "^8.1.0",
- **typescript**        : "^3.4.5"

